import axiosApi from "../../../axiosApi";

export const DISH_SUCCESS = "DISH_SUCCESS";
export const DISH_REQUEST = "DISH_REQUEST";
export const DISH_FAILURE = "DISH_FAILURE";

export const REMOVE_SUCCESS = "REMOVE_SUCCESS";
export const REMOVE_REQUEST = "REMOVE_REQUEST";
export const REMOVE_FAILURE = "REMOVE_FAILURE";

export const FETCH_DISHES_SUCCESS = "FETCH_DISHES_SUCCESS";
export const FETCH_DISHES_REQUEST = "FETCH_DISHES_REQUEST";
export const FETCH_DISHES_FAILURE = "FETCH_DISHES_FAILURE";

export const EDIT_SUCCESS = "EDIT_SUCCESS";
export const EDIT_REQUEST = "EDIT_REQUEST";
export const EDIT_FAILURE = "EDIT_FAILURE";

export const GET_DISH_SUCCESS = "GET_DISH_SUCCESS";
export const GET_DISH_REQUEST = "GET_DISH_REQUEST";
export const GET_DISH_FAILURE = "GET_DISH_FAILURE";

export const dishSuccess = () => ({type: DISH_SUCCESS});
export const dishRequest = () => ({type: DISH_REQUEST});
export const dishFailure = error => ({type: DISH_FAILURE, payload: error});

export const removeSuccess = () => ({type: REMOVE_SUCCESS});
export const removeRequest = () => ({type: REMOVE_REQUEST});
export const removeFailure = e => ({type: REMOVE_FAILURE, payload: e});

export const fetchDishSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, payload: dishes});
export const fetchDishRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishFailure = error => ({type: FETCH_DISHES_FAILURE, payload: error});

export const editSuccess = () => ({type: EDIT_SUCCESS});
export const editRequest = () => ({type: EDIT_REQUEST});
export const editFailure = e => ({type: EDIT_FAILURE, payload: e});

export const getDishSuccess = dish => ({type: GET_DISH_SUCCESS, payload: dish});
export const getDishRequest = () => ({type: GET_DISH_REQUEST});
export const getDishFailure = e => ({type: GET_DISH_FAILURE, payload: e});

export const createDish = (dish) => async (dispatch) => {
    try {
        dispatch(dishRequest());
        await axiosApi.post("/dishes.json", dish);
        dispatch(dishSuccess());
    } catch (e) {
        dispatch(dishFailure(e));
        throw e;
    }
}

export const getDishes = () => async (dispatch) => {
    try {
        dispatch(fetchDishRequest());
        const {data} = await axiosApi.get("/dishes.json");
        dispatch(fetchDishSuccess(data));
    } catch (e) {
        dispatch(fetchDishFailure(e));
        throw e;
    }
}

export const removeDish = dish => async (dispatch) => {
    try {
        dispatch(removeRequest());
        await axiosApi.delete(`/dishes/${dish}.json`);
        dispatch(removeSuccess());
    } catch (e) {
        dispatch(removeFailure(e));
        throw e;
    }
}

export const putDish = (dish, newDish) => async (dispatch) => {
    try {
        dispatch(editRequest());
        await axiosApi.put(`/dishes/${dish}.json`, newDish);
        dispatch(editSuccess());
    } catch (e) {
        dispatch(editFailure(e));
        throw e;
    }
}

export const getDish = (dish) => async (dispatch) => {
    try {
        dispatch(getDishRequest());
        const {data} = await axiosApi.get(`/dishes/${dish}.json`);
        dispatch(getDishSuccess(data));
    } catch (e) {
        dispatch(getDishFailure(e));
        throw e;
    }
}