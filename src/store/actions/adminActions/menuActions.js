import axiosApi from "../../../axiosApi";

export const GET_MENU_REQUEST = "GET_MENU_REQUEST";
export const GET_MENU_SUCCESS = "GET_MENU_SUCCESS";
export const GET_MENU_FAILURE = "GET_MENU_FAILURE";

export const ADD_TO_BASKET = "ADD_TO_BASKET";
export const REMOVE_FROM_BASKET = "REMOVE_FROM_BASKET";

export const GET_ORDERS_SUCCESS = "GET_ORDERS_SUCCESS";
export const GET_ORDERS_REQUEST = "GET_ORDERS_REQUEST";
export const GET_ORDERS_FAILURE = "GET_ORDERS_FAILURE";

export const getMenuRequest = () => ({type: GET_MENU_REQUEST});
export const getMenuSuccess = menu => ({type: GET_MENU_SUCCESS, payload: menu});
export const getMenuFailure = e => ({type: GET_MENU_FAILURE, payload: e});

export const addItem = (state, price) => ({type: ADD_TO_BASKET, payload: state, price});
export const removeItem = (state ,price) => ({type: REMOVE_FROM_BASKET, payload: state, price});

export const getOrdersSuccess = (orders) => ({type: GET_ORDERS_SUCCESS, payload: orders});
export const getOrdersRequest = () => ({type: GET_ORDERS_REQUEST});
export const getOrdersFailure = (e) => ({type: GET_ORDERS_FAILURE, payload: e});

export const getMenu = () => async (dispatch) => {
    try {
        dispatch(getMenuRequest());
        const {data} = await axiosApi.get(`/dishes.json`);
        dispatch(getMenuSuccess(data));
    } catch (e) {
        dispatch(getMenuFailure(e));
        throw e;
    }
}

export const addToBasketItem = dish => (dispatch, state) => {
    const newState = [...state().menu.basket];
    const itemIndex = newState.findIndex(item => item.title === dish.title);
    if (itemIndex === -1) {
        newState.push({title: dish.title, price: dish.price, amount: 1});
    } else {
        newState[itemIndex].amount++;
    }
    dispatch(addItem(newState, dish.price));
}

export const removeFromBasket = dish => (dispatch, state) => {
    const newState = [...state().menu.basket];
    const itemIndex = newState.findIndex(item => item.title === dish.title);
    if (itemIndex === -1) {
        newState.push({title: dish.title, price: dish.price, amount: 1});
    } else if (newState[itemIndex].amount <= 1) {
        newState.splice(itemIndex, 1);
    } else {
        newState[itemIndex].amount--;
    }
    dispatch(removeItem(newState, dish.price))
}

export const doOrder = (order) => async () => {
    try {
        await axiosApi.post("/orders.json", order);
    } catch (e) {
        console.log(e);
    }
}

export const getOrders = () => async (dispatch) => {
    try {
        dispatch(getOrdersRequest());
        const {data} = await axiosApi.get(`/orders.json`);
        dispatch(getOrdersSuccess(data));
    } catch (e) {
        dispatch(getOrdersFailure(e));
    }
}
