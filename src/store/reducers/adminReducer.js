import {
    DISH_FAILURE,
    DISH_REQUEST,
    DISH_SUCCESS,
    EDIT_FAILURE,
    EDIT_REQUEST,
    EDIT_SUCCESS,
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    GET_DISH_FAILURE,
    GET_DISH_REQUEST,
    GET_DISH_SUCCESS,
    REMOVE_FAILURE,
    REMOVE_REQUEST,
    REMOVE_SUCCESS
} from "../actions/adminActions/adminActions";

const initialState = {
    dishes: [],
    dish: {},
    error: null,
    fetchError: null,
    removeError: null,
    editError: null,
    gotDishError: null
}

const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISH_REQUEST:
            return {...state};
        case DISH_SUCCESS:
            return {...state};
        case DISH_FAILURE:
            return {...state, error: action.payload};
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.payload};
        case FETCH_DISHES_REQUEST:
            return {...state};
        case FETCH_DISHES_FAILURE:
            return {...state, fetchError: action.payload};
        case REMOVE_SUCCESS:
            return {...state};
        case REMOVE_REQUEST:
            return {...state};
        case REMOVE_FAILURE:
            return {...state, removeError: action.payload};
        case EDIT_SUCCESS:
            return {...state};
        case EDIT_REQUEST:
            return {...state};
        case EDIT_FAILURE:
            return {...state, editError: action.payload};
        case GET_DISH_SUCCESS:
            return {...state, dish: action.payload};
        case GET_DISH_REQUEST:
            return {...state};
        case GET_DISH_FAILURE:
            return {...state, gotDishError: action.payload};
        default:
            return state;
    }
}

export default adminReducer;