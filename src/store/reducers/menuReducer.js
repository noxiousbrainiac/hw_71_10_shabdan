import {
    ADD_TO_BASKET,
    GET_MENU_FAILURE,
    GET_MENU_REQUEST,
    GET_MENU_SUCCESS, GET_ORDERS_FAILURE, GET_ORDERS_REQUEST, GET_ORDERS_SUCCESS,
    REMOVE_FROM_BASKET
} from "../actions/adminActions/menuActions";

const initialState = {
    menu: [],
    error: null,
    basket: [],
    totalPrice: 150,
    orders: [],
    ordersError: null
}

const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MENU_REQUEST:
            return {...state};
        case GET_MENU_SUCCESS:
            return {...state, menu: action.payload};
        case GET_MENU_FAILURE:
            return {...state, error: action.payload};
        case ADD_TO_BASKET:
            return {
                ...state,
                basket: action.payload,
                totalPrice: state.totalPrice + parseInt(action.price),
            };
        case REMOVE_FROM_BASKET:
            return {
                ...state,
                basket: action.payload,
                totalPrice: state.totalPrice - parseInt(action.price),
            };
        case GET_ORDERS_REQUEST:
            return {...state};
        case GET_ORDERS_SUCCESS:
            return {...state, orders: action.payload};
        case GET_ORDERS_FAILURE:
            return {...state, ordersError: action.payload};
        default:
            return state;
    }
}

export default menuReducer;