import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import {BrowserRouter} from "react-router-dom";
import adminReducer from "./store/reducers/adminReducer";
import "./index.css";
import menuReducer from "./store/reducers/menuReducer";

const rootReducer = combineReducers({
    admin: adminReducer,
    menu: menuReducer
})

const store = createStore(rootReducer ,applyMiddleware(thunk));

const app = (
    <BrowserRouter>
        <Provider store={store}>
            <App/>
        </Provider>
    </BrowserRouter>
)

ReactDOM.render(app, document.getElementById('root'));
