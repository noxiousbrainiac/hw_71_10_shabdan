import React, {useEffect, useState} from 'react';
import {Button, Container, makeStyles} from "@material-ui/core";
import MenuItems from "../../components/TurtleMenu/MenuItems/MenuItems";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {addToBasketItem, doOrder, getMenu, removeFromBasket} from "../../store/actions/adminActions/menuActions";
import Basket from "../../components/TurtleMenu/Basket/Basket";
import CheckOut from "../../components/TurtleMenu/CheckOut/CheckOut";

const useStyles = makeStyles({
    mainBlock: {
        display: "flex",
        paddingTop: "30px"
    }
})

const TurtleMenu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const {menu, basket, totalPrice} = useSelector(state => ({
        menu: state.menu.menu,
        basket: state.menu.basket,
        totalPrice: state.menu.totalPrice
    }), shallowEqual);

    const toBasket = (dish) => {
        dispatch(addToBasketItem(dish, basket));
    }

    const remove = (dish) => {
        dispatch(removeFromBasket(dish, basket, menu))
    }

    const handleClose = () => setOpen(false);

    const checkOut = () => {
        setOpen(true);
    }

    const createOrder = async () => {
        await dispatch(doOrder(basket));
        setOpen(false);
    }

    useEffect(() => {
        dispatch(getMenu());
    }, [dispatch])

    return (
        <Container>
            <div className={classes.mainBlock}>
                <MenuItems menu={menu} onclick={toBasket}/>
                <Basket basket={basket} totalPrice={totalPrice} remove={remove} menu={menu} checkout={checkOut}/>
            </div>
            <CheckOut
                open={open}
                close={handleClose}
                cancel={handleClose}
                order={createOrder}
            >
                <ul style={{padding: "0", listStyle: "none"}}>
                    {
                        basket.length !== 0
                            ? basket.map((order, i) => (
                                <li key={i} style={{display: "flex"}}>
                                    <b style={{marginRight: "5px"}}>{order.amount}x</b>
                                    <span style={{marginRight: "5px", flexGrow: "1"}}>{order.title}</span>
                                    <span style={{marginRight: "5px"}}><b>{order.price}</b> som</span>
                                    <Button size="small" onClick={() => remove(order, basket, menu)}>x</Button>
                                </li>))
                            : <h3>Empty basket</h3>
                    }
                </ul>
            </CheckOut>
        </Container>
    );
};

export default TurtleMenu;