import React, {useEffect} from 'react';
import {Button, Container, makeStyles} from "@material-ui/core";
import Dishes from "../../components/TurtleAdminComponents/Dishes/Dishes";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {getDishes, removeDish} from "../../store/actions/adminActions/adminActions";

const useStyles = makeStyles({
    adminHeader: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
    }
})

const TurtleAdminContainer = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const {dishes} = useSelector(state => ({
        dishes: state.admin.dishes
    }), shallowEqual);

    const addDishForm = () => {
        history.push("/addDishForm");
    }

    const deleteDish = async (dish) => {
        await dispatch(removeDish(dish));
        await dispatch(getDishes());
    }

    const toEditPage = (id) => {
        history.push(`/editPage/${id}`);
    }

    useEffect(() => {
        dispatch(getDishes());
    }, [dispatch])

    return (
        <Container>
            <div className={classes.adminHeader}>
                <h1>Dishes</h1>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={addDishForm}
                >
                    Add new Dish
                </Button>
            </div>
            <Dishes dishes={dishes} remove={deleteDish} edit={toEditPage}/>
        </Container>
    );
};

export default TurtleAdminContainer;