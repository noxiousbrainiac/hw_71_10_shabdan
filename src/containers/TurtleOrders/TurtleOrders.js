import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getOrders} from "../../store/actions/adminActions/menuActions";

const TurtleOrders = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.menu.orders);
    useEffect(() => {
        dispatch(getOrders());
    }, [dispatch]);

    return (
        <>

        </>
    );
};

export default TurtleOrders;