import React, {useEffect, useState} from 'react';
import {Button, Container, makeStyles, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {useParams} from "react-router-dom/cjs/react-router-dom";
import {getDish, putDish} from "../../../store/actions/adminActions/adminActions";

const useStyles = makeStyles({
    formItems: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        padding: "20px 0"
    },
    formItem: {
        marginBottom: "15px"
    },
    btn: {
        alignSelf: "center"
    }
})

const DishEdit = () => {
    const classes = useStyles();
    const params = useParams();
    const dispatch = useDispatch();
    const history = useHistory();
    const dish = useSelector(state => state.admin.dish);
    const [input, setInput] = useState({
        title: "",
        price: "",
        image: ""
    })

    const inputHandle = e => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const saveInfo = async () => {
        try {
            await dispatch(putDish(params.id ,input));
            history.push(`/`);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        dispatch(getDish(params.id));
        dish.title && setInput(prev => ({
            ...prev,
            title: dish.title,
            price: dish.price,
            image: dish.image
        }));
    }, [dispatch, dish.title, dish.image, dish.price, params.id]);

    return  (
        <Container>
            <form>
                <div className={classes.formItems}>
                    <TextField
                        className={classes.formItem}
                        value={input.title}
                        onChange={e => inputHandle(e)}
                        size="small"
                        label="Title"
                        name="title"
                        variant="filled"
                    />
                    <TextField
                        className={classes.formItem}
                        value={input.price}
                        onChange={e => inputHandle(e)}
                        size="small"
                        label="Price"
                        name="price"
                        variant="filled"
                    />
                    <TextField
                        className={classes.formItem}
                        value={input.image}
                        onChange={e => inputHandle(e)}
                        size="small"
                        label="Img"
                        name="image"
                        variant="filled"
                    />
                    <Button
                        className={classes.btn}
                        onClick={saveInfo}
                        variant="contained"
                        color="secondary"
                    >
                        Save
                    </Button>
                </div>
            </form>
        </Container>
    );
};

export default DishEdit;