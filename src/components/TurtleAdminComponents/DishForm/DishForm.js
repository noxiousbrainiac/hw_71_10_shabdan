import React, {useState} from 'react';
import {Button, Container, makeStyles, TextField} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createDish} from "../../../store/actions/adminActions/adminActions";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles({
    formItems: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        padding: "20px 0"
    },
    formItem: {
        marginBottom: "15px"
    },
    btn: {
        alignSelf: "center"
    }
})

const DishForm = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const [input, setInput] = useState({
        title: "",
        price: "",
        image: ""
    });

    const inputHandle = e => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const createDishClick = async e => {
        e.preventDefault();

        try {
            if (input.title.length > 0 && input.price.length > 0 && input.image.length > 0) {
                await dispatch(createDish(input));
            }
            history.push("");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Container>
            <form>
                <div className={classes.formItems}>
                    <TextField
                        className={classes.formItem}
                        value={input.title}
                        onChange={inputHandle}
                        size="small"
                        label="Title"
                        name="title"
                        variant="filled"
                    />
                    <TextField
                        className={classes.formItem}
                        value={input.price}
                        onChange={inputHandle}
                        size="small"
                        label="Price"
                        name="price"
                        variant="filled"
                    />
                    <TextField
                        className={classes.formItem}
                        value={input.image}
                        onChange={inputHandle}
                        size="small"
                        label="Img"
                        name="image"
                        variant="filled"
                    />
                    <Button
                        onClick={createDishClick}
                        className={classes.btn}
                        variant="contained"
                        color="secondary"
                    >
                        Add
                    </Button>
                </div>
            </form>
        </Container>
    );
};

export default DishForm;