import React from 'react';
import Dish from "./Dish/Dish";

const Dishes = ({dishes, remove, edit}) => {

    return (
        <div>
            <ul>
                {dishes
                    ? Object.keys(dishes).map((dish, i) => (
                    <Dish
                        key={i}
                        price={dishes[dish].price}
                        dishName={dishes[dish].title}
                        imgUrl={dishes[dish].image}
                        remove={() => remove(dish)}
                        edit={() => edit(dish)}
                    />
                ))
                    : <h3>No dishes here, please add !</h3>
                }
            </ul>
        </div>
    );
};

export default Dishes;