import React from 'react';
import {Button, Card, makeStyles} from "@material-ui/core";

const useStyle = makeStyles({
    liContainer: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "10px",
        marginBottom: "10px"
    },
    dishImg: {
        width: "80px",
        display: "block",
        alignContent: "center",
        marginRight: "50px"
    },
    liText: {
        margin: "0",
        textTransform: "capitalize",
        flexGrow: "1",
        fontSize: "21px"
    },
    btn: {
        marginLeft: "30px"
    }
})

const Dish = ({dishName, price, imgUrl, remove, edit}) => {
    const classes = useStyle();

    return (
        <Card className={classes.liContainer}>
            <span className={classes.dishImg}>
                <img src={imgUrl} alt="" style={{width: "100%"}}/>
            </span>
            <span className={classes.liText}>{dishName}</span>
            <b>{price} som</b>
            <Button
                className={classes.btn}
                variant="contained"
                color="secondary"
                onClick={edit}
            >
                Edit
            </Button>
            <Button
                className={classes.btn}
                variant="contained"
                color="secondary"
                onClick={remove}
            >
                Delete
            </Button>
        </Card>
    );
};

export default Dish;