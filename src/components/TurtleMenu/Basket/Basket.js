import React from 'react';
import {Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    basket: {
        width: "23%",
        paddingLeft: "20px"
    }
})

const Basket = ({basket, totalPrice, remove, menu, checkout}) => {
    const classes = useStyles();

    return (
        <div className={classes.basket}>
            <h4>Your basket</h4>
            <ul style={{padding: "0", listStyle: "none"}}>
                {
                    basket.length !== 0
                        ? basket.map((order, i) => (
                            <li key={i} style={{display: "flex", flexWrap: "wrap"}}>
                                <b style={{marginRight: "5px"}}>{order.amount}x</b>
                                <span style={{marginRight: "5px", flexGrow: "1"}}>{order.title}</span>
                                <span style={{marginRight: "5px"}}><b>{order.price}</b> som</span>
                                <Button size="small" onClick={() => remove(order, basket, menu)}>x</Button>
                            </li>))
                        : <h3>Empty basket</h3>
                }
            </ul>
            <h3>Total: {totalPrice} som</h3>
            <Button
                variant="contained"
                color="secondary"
                onClick={() => checkout()}
            >
                Check out
            </Button>
        </div>
    );
};

export default Basket;