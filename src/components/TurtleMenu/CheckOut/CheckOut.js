import React from 'react';
import {Backdrop, Button, Card, Fade, makeStyles, Modal} from "@material-ui/core";

const useStyle = makeStyles({
    card: {
        width: "400px",
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)",
        padding: "20px"
    },
    btn: {
        marginRight: "20px"
    }
})

const CheckOut = ({open, close, children, order, cancel}) => {
    const classes = useStyle();

    return (
        <Modal
            open={open}
            onClose={() => close()}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={open}>
                <Card className={classes.card}>
                    {children}
                    <div>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            color="secondary"
                            onClick={() => order()}
                        >
                            Order
                        </Button>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            color="secondary"
                            onClick={() => cancel()}
                        >
                            Cancel
                        </Button>
                    </div>
                </Card>
            </Fade>
        </Modal>
    );
};

export default CheckOut;