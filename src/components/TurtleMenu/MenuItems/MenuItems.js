import React from 'react';
import {makeStyles} from "@material-ui/core";
import MenuItem from "./MenuItem/MenuItem";

const useStyles = makeStyles({
    itemsBox: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        width: "80%"
    }
});

const MenuItems = ({menu, onclick}) => {
    const classes = useStyles();

    return (
         <div className={classes.itemsBox}>
                {
                    menu.length !== 0
                    ? Object.keys(menu).map((dish, i) => (
                        <MenuItem
                            key={i}
                            image={menu[dish].image}
                            title={menu[dish].title}
                            price={menu[dish].price}
                            onClick={() => onclick(menu[dish])}
                        />))
                        : <h2>No dishes in Menu :( </h2>
                }
        </div>
    );
};

export default MenuItems;