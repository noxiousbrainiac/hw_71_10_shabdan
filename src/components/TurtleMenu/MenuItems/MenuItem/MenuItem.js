import React from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, Typography} from "@material-ui/core";

const MenuItem = ({image, title, price, onClick}) => {
    return (
        <Card style={{width: "300px", marginBottom: "25px"} }>
            <CardMedia
                component="img"
                width="100%"
                height="140"
                image={image}
                alt="dish"
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {title}
                </Typography>
                <b>{price}</b>
            </CardContent>
            <CardActions>
                <Button
                    onClick={onClick}
                    size="small"
                    variant="contained"
                    color="primary"
                >
                    Add to basket
                </Button>
            </CardActions>
        </Card>
    );
};

export default MenuItem;