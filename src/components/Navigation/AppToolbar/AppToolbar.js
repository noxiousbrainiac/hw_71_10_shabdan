import React from 'react';
import {makeStyles, Toolbar} from "@material-ui/core";
import NavigationItems from "../NavigationItems/NavigationItems";
import {NavLink} from "react-router-dom";

const useStyle = makeStyles({
    Toolbar: {
        background: "#1976d2",
        justifyContent: "space-between"
    },
    title: {
        color: "beige",
        margin: "0 0 0 5px"
    },
    logoImg: {
        width: "100%"
    },
    logo: {
        display: "flex",
        alignItems: "center",
        textDecoration: "none"
    }
})

const AppToolbar = () => {
    const classes = useStyle();

    return (
        <Toolbar className={classes.Toolbar}>
            <NavLink to="/" className={classes.logo}>
                <div style={{width: "40px"}}>
                    <img className={classes.logoImg} src="https://cdn-icons-png.flaticon.com/512/1404/1404945.png" alt=""/>
                </div>
                <h3 className={classes.title}>Turtle Pizza</h3>
            </NavLink>
            <NavigationItems/>
        </Toolbar>
    );
};

export default AppToolbar;