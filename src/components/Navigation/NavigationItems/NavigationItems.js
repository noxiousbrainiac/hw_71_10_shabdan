import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    list: {
        display: "flex",
        listStyle: "none"
    }
})

const NavigationItems = () => {
    const classes = useStyles();

    return (
        <ul className={classes.list}>
            <NavigationItem to="/menu" >Menu</NavigationItem>
            <NavigationItem to="/orders">Orders</NavigationItem>
            <NavigationItem to="/" exact>Dishes</NavigationItem>
        </ul>
    );
};

export default NavigationItems;