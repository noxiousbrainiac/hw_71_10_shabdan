import React from 'react';
import {Route, Switch} from "react-router-dom";
import TurtleAdminContainer from "./containers/TurtleAdminContainer/TurtleAdminContainer";
import AppToolbar from "./components/Navigation/AppToolbar/AppToolbar";
import TurtleMenu from "./containers/TurtleMenu/TurtleMenu";
import DishForm from "./components/TurtleAdminComponents/DishForm/DishForm";
import DishEdit from "./components/TurtleAdminComponents/DishEdit/DishEdit";
import TurtleOrders from "./containers/TurtleOrders/TurtleOrders";

const App = () => {
    return (
        <>
            <AppToolbar/>
            <Switch>
                <Route exact path="/" component={TurtleAdminContainer}/>
                <Route path="/editPage/:id" component={DishEdit}/>
                <Route path="/addDishForm" component={DishForm}/>
                <Route path="/orders" component={TurtleOrders}/>
                <Route path="/menu" component={TurtleMenu}/>
            </Switch>
        </>
    );
};

export default App;